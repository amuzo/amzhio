﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UISystem : MonoBehaviour {
    public static UISystem instance;
    public Text ScoreText;
    public GameObject ScoreTextObj;
    public Text TopScoreText;
    public GameObject TopScoreTextObj;
    public GameObject InputText;
    public InputField Playername;
    // Use this for initialization
    void Start () {
		if (instance == null)
        {
            instance = this;
        }else
        {
            Destroy(this.gameObject);
        }
	}
	
	// Update is called once per frame
	void Update () {
        ScoreText.text = (GameController.instance.PreviousScore.ToString());
        TopScoreText.text = (GameController.instance.TopScore.ToString());

    }
}
