﻿using UnityEngine;
using System.Collections;

public class DownloadRequest_LocalOnly : DownloadRequest 
{
	public DownloadRequest_LocalOnly() : base()
	{
		doRemoteDownload = false;
		
		doCacheDownload = false;
		doCacheHeaderInfo = false;
		
		doLocalDownload = true;
		doLocalHeaderInfo = true;
	}
}
