﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    public static Spawner instance;
    public GameObject[] Pieces;
    public float TriggerPoint;
    public float speed;
    public float CurCounter;
    public float PieceSpeed;
    public int piecetospawn;

    public int PiecesToSpawn;
    public int CurrentSpawned;

    public List<float> playerscores = new List<float>();


    private bool Triggered;

    public bool Player1Joined;
    public bool Player2Joined;
    public bool Player3Joined;
    public bool Player4Joined;

    public bool startspawning;
    public GameObject FinishLine;
    public GameObject[] Players;
    private bool counted;
    public int numtospawn;
    public bool Done;
	// Use this for initialization
	void Start () {
		if (instance == null)
        {
            instance = this;
        }
        numtospawn = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (Triggered)
        {
            Debug.Log("I should not be able to do this");
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Triggered = true;
        }
        if (PhotonNetwork.playerList.Length > GameController.instance.PlayersBeforeGo)
        {
            PhotonNetwork.room.IsOpen = false;
            startspawning = true;
            PhotonNetwork.room.IsVisible = false;
    


        }

        //  Debug.Log(PhotonNetwork.room.MasterClientId);
        if (numtospawn == Pieces.Length)
        {
            numtospawn = 0;
        }
        if (startspawning)
        {

            if (CurCounter >= TriggerPoint && Done == false)
            {
                //  piecetospawn = Random.Range(0, Pieces.Length);
                SpawnThing();
               // photonView.RPC("SpawnThing", PhotonTargets.All);

            }
            if (CurCounter < TriggerPoint)
            {
                CurCounter = CurCounter + speed * Time.deltaTime;
            }
        }
	}



   
   
    private void SpawnThing()
    {
        if (CurrentSpawned < PiecesToSpawn)
            {

      
            CurCounter = 0f;
            PieceSpeed = PieceSpeed + 0.2f;
            TriggerPoint = TriggerPoint - 0.2f;
            if (TriggerPoint < 0.4f)
            {
                TriggerPoint = 0.4f;
          
            }
            if (PieceSpeed > 6)
            {
                PieceSpeed = 6;
            }
        
            Instantiate(Pieces[numtospawn], transform.position, Quaternion.identity);
            numtospawn++;
            CurrentSpawned++;
        }

        if(CurrentSpawned == PiecesToSpawn && Done == false)
        {
            Instantiate(FinishLine, transform.position, Quaternion.identity);
            Done = true;
        }
    }
}
