﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockPicker : MonoBehaviour {
    public GameObject[] Rocks;
	// Use this for initialization
	void Start () {
        int item = Random.Range(0, Rocks.Length);
        Instantiate(Rocks[item], transform.position, Quaternion.identity, this.transform);
	}

}
