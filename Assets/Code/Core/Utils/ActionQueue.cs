﻿////////////////////////////////////////////
// 
// ActionQueue.cs
//
// Created: 18/07/2014 ccuthbert
// Contributors:
// 
// Intention:
// Manage a queue of actions, allowing async 
// performance of the actions.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using System.Collections.Generic;
using Logger = UnityEngine.Debug;
using Verbose = UnityEngine.Debug;

namespace AmuzoEngine
{

public sealed class ActionQueue
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[ActionQueue] ";

	private const string	QUEUE_NAME_DEFAULT = "unnamed queue";

	private const string	ACTION_NAME_DEFAULT = "unnamed action";

	private enum EActionState
	{
		NULL = 0,
		WAITING,
		ACTIVE,
		COMPLETE,
		CANCELLED
	}

	public enum EActionStart
	{
		LAST = 0,
		NEXT,
		NOW
	}

	public enum EActionRemovePolicy
	{
		ON_ALL_ENDED = 0,
		ON_ONE_ENDED,
		UNMANAGED
	}

	//
	// SINGLETON DECLARATION
	//

	private static SimpleIntegerIdGenerator	_sActionIdGenerator = new SimpleIntegerIdGenerator();

	//
	// NESTED CLASSES / STRUCTS
	//

	private class Node
	{
		public int	_actionId;
		public System.Action	_action;
		public bool	_isAsync;
		public string	_name;
		public EActionState	_state;
		public Node( int actionId, System.Action action, bool isAsync, string name )
		{
			_actionId = actionId;
			_action = action;
			_isAsync = isAsync;
			_name = name ?? ACTION_NAME_DEFAULT;
			_state = EActionState.NULL;
		}
	}
	
	//
	// MEMBERS 
	//
	
	private string	_name;

	private EActionRemovePolicy	_removePolicy;
	
	private	List<Node>	_nodes;

	private Dictionary<object, List<int>>	_actionGroups;
	
	private int	_nextNode;

	private int	_numActive;
	
	//
	// PROPERTIES
	//
	
	private string	_pLogTag	{ get { return "[ActionQueue:" + _name + "] "; } } 
	
	private bool _pIsNextAction
	{
		get
		{
			return _nextNode < _nodes.Count;
		}
	}
	
	public bool	_pIsEmpty	{ get { return _nodes == null || _nextNode == _nodes.Count; } }

	public bool	_pIsAllActionsEnded { get { return _pIsEmpty && _numActive == 0; } }
	
	public int	_pTotalCount	{ get { return _nodes != null ? _nodes.Count : 0; } }
	
	public int	_pActiveCount	{ get { return _numActive; } }

	public event System.Action<int>	_pOnActionStarted;

	public event System.Action<int>	_pOnActionEnded;

	public event System.Action	_pOnAllActionsEnded;
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// CONSTRUCTORS
	//
	
	public ActionQueue() : this( QUEUE_NAME_DEFAULT )
	{
	}
	
	public ActionQueue( string name )
	{
		_name = name;

		Reset();
	}
	
	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//

	public void SetActionRemovePolicy( EActionRemovePolicy removePolicy )
	{
		_removePolicy = removePolicy;
	}
	
	public int AddAction( System.Action action, System.Action<int> onActionAdded = null, bool isAsyncAction = false, string name = null, object groupId = null, EActionStart start = EActionStart.LAST )
	{
		if ( _nodes == null ) return -1;
		if ( action == null ) return -1;
//		Verbose.Log( _pLogTag + "Adding action[" + _nodes.Count.ToString() + "]" ); WARNING: THESE LOGS TAKE A LOAD OF TIME WITH 1000s OF ACTIONS
		int	actionId = _sActionIdGenerator._pNew;

		Node	newNode = new Node( actionId, action, isAsyncAction, name );

		if ( groupId != null )
		{
			SetActionGroup( newNode, groupId );
		}

		SetActionState( newNode, EActionState.WAITING );

		if ( start == EActionStart.LAST )
		{
			_nodes.Add( newNode );
		}
		else
		{
			_nodes.Insert( _nextNode, newNode );
		}

		if ( onActionAdded != null )
		{
			onActionAdded( actionId );
		}

		if ( start == EActionStart.NOW )
		{
			DoNextAction();
		}

		return actionId;
	}
	
	public void OnActionComplete( int actionId )
	{
		Node	node = FindNode( actionId );

		if ( node == null )
		{
			Logger.LogWarning( _pLogTag + "Failed to find action " + actionId );
			return;
		}

		if ( node._state == EActionState.CANCELLED ) return;

		bool	wasStarted = node._state == EActionState.ACTIVE;

		if ( !wasStarted )
		{
			Logger.LogError( _pLogTag + "Unexpected action state: " + node._state );
			return;
		}

		SetActionState( node, EActionState.COMPLETE );
	}
	
	public void DoActions( System.Func<bool> isNextActionAllowed = null )
	{
		if ( _nodes == null ) return;
		if ( isNextActionAllowed == null )
		{
			isNextActionAllowed = () => true;
		}
		
		while ( _pIsNextAction && isNextActionAllowed() )
		{
			DoNextAction();
		}
	}
	
	public void DoActions( int maxActions )
	{
		int	actionCounter = _numActive;
		
		DoActions( () => ( actionCounter++ < maxActions ) );
	}
	
	public void DoActions( float maxSeconds, System.Func<float> getSystemSeconds )
	{
		float	actionMaxTime = 0.01f;
		float	actionStartTime = getSystemSeconds();
		
		DoActions( () => ( getSystemSeconds() - actionStartTime < actionMaxTime ) );
	}
	
	public void Reset()
	{
		_nodes = new List<Node>();
		_actionGroups = new Dictionary<object, List<int>>();
		_nextNode = 0;
		_numActive = 0;
	}

	public void CancelAction( int actionId )
	{
		Node	node = FindNode( actionId );

		if ( node == null ) return;

		SetActionState( node, EActionState.CANCELLED );
	}

	public void CancelActions( object groupId )
	{
		List<int>	groupList = FindActionGroup( groupId );

		if ( groupList == null ) return;

		for ( int i = 0; i < groupList.Count; ++i )
		{
			CancelAction( groupList[i] );
		}
	}

	public bool IsAllActionsEnded( object groupId )
	{
		if ( groupId == null ) return _pIsAllActionsEnded;

		List<int>	groupList = FindActionGroup( groupId );

		if ( groupList == null ) return true;

		bool	isAllComplete = true;
		Node	node;

		for ( int i = 0; i < groupList.Count && isAllComplete; ++i )
		{
			node = FindNode( groupList[i] );

			if ( node == null ) continue;

			isAllComplete = node._state == EActionState.COMPLETE || node._state == EActionState.CANCELLED;
		}

		return isAllComplete;
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private void DoNextAction()
	{
//		Verbose.Log( _pLogTag + "Doing action[" + _nextNode.ToString() + "]: " + _nodes[ _nextNode ]._name ); WARNING: THESE LOGS TAKE A LOAD OF TIME WITH 1000s OF ACTIONS
		Node	node = _nodes[ _nextNode++ ];

		if ( node == null )
		{
			Logger.LogError( _pLogTag + "Null node!" );
			return;
		}

		if ( node._state == EActionState.CANCELLED ) return;

		if ( node._state != EActionState.WAITING )
		{
			Logger.LogError( _pLogTag + "Unexpected action state: " + node._state );
			return;
		}

		SetActionState( node, EActionState.ACTIVE );

		node._action();

		if ( !node._isAsync )
		{
			SetActionState( node, EActionState.COMPLETE );
		}
	}

	private void SetActionState( Node node, EActionState newState )
	{
		if ( node._state == newState ) return;

		if ( node._state == EActionState.ACTIVE )
		{
			--_numActive;
		}

		node._state = newState;

		switch ( node._state )
		{
		case EActionState.ACTIVE:
			++_numActive;
			OnActionStarted( node );
			break;
		case EActionState.COMPLETE:
		case EActionState.CANCELLED:
			OnActionEnded( node );
			break;
		}
	}

	private void OnActionStarted( Node node )
	{
		if ( _pOnActionStarted != null )
		{
			_pOnActionStarted( node._actionId );
		}
	}

	private void OnActionEnded( Node node )
	{
		node._action = null;

		if ( _pOnActionEnded != null )
		{
			_pOnActionEnded( node._actionId );
		}

		if ( _pIsAllActionsEnded )
		{
			OnAllActionsEnded();
		}

		if ( _removePolicy == EActionRemovePolicy.ON_ONE_ENDED )
		{
			RemoveAction( node );
		}
	}

	private void OnAllActionsEnded()
	{
		if ( _pOnAllActionsEnded != null )
		{
			_pOnAllActionsEnded();
		}

		if ( _removePolicy == EActionRemovePolicy.ON_ALL_ENDED )
		{
			Reset();
		}
	}
	
	private void RemoveAction( Node node )
	{
		int	nodeIndex = _nodes.IndexOf( node );

		if ( nodeIndex < 0 ) return;

		_nodes.RemoveAt( nodeIndex );

		if ( nodeIndex < _nextNode )
		{
			--_nextNode;
		}
	}
	
	private Node FindNode( int actionId )
	{
		return _nodes.Find( n => n._actionId == actionId );
	}

	private void SetActionGroup( Node node, object groupId )
	{
		List<int>	groupList;

		if ( _actionGroups.ContainsKey( groupId ) )
		{
			groupList = _actionGroups[ groupId ];
		}
		else
		{
			groupList = new List<int>();

			_actionGroups.Add( groupId, groupList );
		}

		groupList.Add( node._actionId );
	}

	private List<int> FindActionGroup( object groupId )
	{
		if ( !_actionGroups.ContainsKey( groupId ) ) return null;

		return _actionGroups[ groupId ];
	}
	
	//
	// OPERATOR OVERLOADS
	//
}

}
