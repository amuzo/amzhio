﻿////////////////////////////////////////////
// 
// Singleton.cs
//
// Created: 19/06/2014 ccuthbert
// Contributors:
// 
// Intention:
// Boilerplate singleton implementation
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.Collections.Generic;

namespace AmuzoEngine
{
	public struct Singleton<T> where T : class
	{	
		private T	_instance;
		
		public T _pInstance
		{
			get { return _instance; }
			set
			{
				if ( value != null )
				{
					Set( value );
				}
				else
				{
					Unset( _instance );
				}
			}
		}
		
		public bool	_pExists	{ get { return _instance != null; } }
		
		public bool Set( T inst )
		{
			return Set( ref _instance, inst );
		}
		
		public bool Unset( T inst )
		{
			return Unset( ref _instance, inst );
		}
		
		public static bool Set( ref T instanceRef, T inst )
		{
			if ( inst == null ) return false;
			if ( instanceRef != null ) return false;
			instanceRef = inst;
			return true;
		}
		
		public static bool Unset( ref T instanceRef, T inst )
		{
			if ( inst == null ) return false;
			if ( instanceRef != inst ) return false;
			instanceRef = null;
			return true;
		}
	}
}

