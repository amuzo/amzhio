﻿////////////////////////////////////////////
// 
// AudioListenerPriority.cs
//
// Created: 21/10/2014 ccuthbert
// Contributors:
// 
// Intention:
// Add to AudioListener objects to make sure 
// one and only one is active.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using UnityEngine;
using AmuzoEngine;
using System.Collections.Generic;
using Logger = UnityEngine.Debug;

public class AudioListenerPriority : MonoBehaviour
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[AudioListenerPriority] ";

	//
	// SINGLETON DECLARATION
	//
	
	private static List<AudioListenerPriority>	_instances = new List<AudioListenerPriority>();
	
	private static int	_noRefreshCounter = 0;

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	public int	_priority;
	
	private AudioListener	_listener;
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//
	
	private static void RegisterInstance( AudioListenerPriority listener )
	{
		if ( _instances == null ) return;
		if ( _instances.Contains( listener ) ) return;
		_instances.Add( listener );
	}
	
	private static void UnregisterInstance( AudioListenerPriority listener )
	{
		if ( _instances == null ) return;
		if ( !_instances.Contains( listener ) ) return;
		_instances.Remove( listener );
	}
	
	private static void RefreshEnabledInstance()
	{
		if ( _instances == null ) return;
		if ( _noRefreshCounter > 0 ) return;
		
		int?	wantListener = null;
		
		for ( int i = 0; i < _instances.Count; ++i )
		{
			if ( _instances[i] == null ) continue;
			if ( !_instances[i].enabled ) continue;
			if ( !_instances[i].gameObject.activeInHierarchy ) continue;
			if ( wantListener.HasValue && _instances[i]._priority <= _instances[wantListener.Value]._priority ) continue;
			wantListener = i;
		}
		
		_noRefreshCounter++;
		
		for ( int i = 0; i < _instances.Count; ++i )
		{
			if ( _instances[i] == null ) continue;
			if ( wantListener.HasValue && i == wantListener.Value )
			{
				_instances[i].SetListenerEnabled( true );
			}
			else
			{
				_instances[i].SetListenerEnabled( false );
			}
		}
		
		_noRefreshCounter--;
	}

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//
	
	void Awake()
	{
		_listener = GetComponent<AudioListener>();
		RegisterInstance( this );
	}
	
	void OnEnable()
	{
		RefreshEnabledInstance();
	}
	
	void OnDisable()
	{
		RefreshEnabledInstance();
	}
	
	void OnDestroy()
	{
		UnregisterInstance( this );
	}

	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	void SetListenerEnabled( bool enabled )
	{
		if ( _listener == null ) return;
		_listener.enabled = enabled;
	}
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

