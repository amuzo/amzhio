//CC20130416

using System;

public static class UnitConversion
{
	// Core values
	
	//length
	private const float	kMetre = 1.0f;
	private const float	kKilometre = 1000.0f * kMetre;
	private const float	kMile = 1609.344f * kMetre;
	
	//time
	private const float	kSecond = 1.0f;
	private const float	kMinute = 60.0f * kSecond;
	private const float	kHour = 60.0f * kMinute;
	
	//mass
	private const float	kKilogram = 1.0f;
	
	//angle
	private const float kPi = (float)Math.PI;
	private const float	kRadian = 1.0f;
	private const float	kDegree = ( kPi / 180.0f ) * kRadian;
	private const float	kRevolution = 360.0f * kDegree;
	
	
	// Conversion multipliers

	//length
	public const float	MilesToMetres = kMile / kMetre;
	public const float	MetresToMiles = kMetre / kMile;
	
	//angle
	public const float	RadToDeg = ( kRadian / kDegree );
	public const float	DegToRad = ( kDegree / kRadian );
	
	//speed
	public const float	MphToMps = ( kMile / kHour ) / ( kMetre / kSecond );
	public const float	MpsToMph = ( kMetre / kSecond ) / ( kMile / kHour );
	
	//angular speed
	public const float	RpmToRps = ( kRevolution / kMinute ) / ( kRadian / kSecond );
	public const float	RpsToRpm = ( kRadian / kSecond ) / ( kRevolution / kMinute );
}
