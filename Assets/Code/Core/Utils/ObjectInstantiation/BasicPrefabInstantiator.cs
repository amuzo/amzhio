﻿////////////////////////////////////////////
// 
// BasicPrefabInstantiator.cs
//
// Created: 01/08/2014 ccuthbert
// Contributors:
// 
// Intention:
// Script to handle basic prefab instantiation.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using UnityEngine;
using Logger = UnityEngine.Debug;

namespace AmuzoEngine
{

public class BasicPrefabInstantiator : MonoBehaviour, IObjectInstantiator
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[BasicPrefabInstantiator] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	public Object	_prefab;
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	//
	// OPERATOR OVERLOADS
	//
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
	
	Object IObjectInstantiator.CreateInstance()
	{
		if ( _prefab == null ) return null;
		
		return Object.Instantiate( _prefab );
	}
	
	void IObjectInstantiator.DestroyInstance( Object inst )
	{
		if ( inst == null ) return;
		
		Object.Destroy( inst );
	}

	bool IObjectInstantiator._pCanReuseInstances	{ get { return false; } }
}

}
