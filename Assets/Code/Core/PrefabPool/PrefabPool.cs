﻿////////////////////////////////////////////
// 
// PrefabPool.cs
//
// Created: dd/mm/yyyy ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using UnityEngine;
using AmuzoEngine;
using System.Collections.Generic;
using Logger = UnityEngine.Debug;

public class PrefabPool : MonoBehaviour, IObjectInstantiator
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[PrefabPool] ";
	
	public enum EInstantiateFailedPolicy
	{
		FAIL = 0,
		USE_OLDEST
	}
	
	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	[System.Serializable]
	public class PrefabInfo
	{
		public Object	_prefab;
		public int		_count;
	}
	
	//
	// MEMBERS 
	//
	
	public EInitializeType	_initType;
	
	public PrefabInfo[]	_prefabs;
	
	public string	_instanceName;
	
	public bool	_isShuffleInstances;
	
	public bool	_isCreateOffline;
	
	public EInstantiateFailedPolicy	_onInstantiateFailed;
	
	public GameObject[]	_instances;
	
	private bool	_isInitialized;
	
	private ObjectPool<GameObject>	_instancePool;
	
	//
	// PROPERTIES
	//
	
	private string	_pLogTag	{ get { return "[PrefabPool:" + gameObject.name + "] "; } }
	
	public bool	_pCanReuseInstances	{ get { return _onInstantiateFailed == EInstantiateFailedPolicy.USE_OLDEST; } }
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	void Awake()
	{
		if ( _initType == EInitializeType.AWAKE )
		{
			Initialize();
		}
	}
	
	void Start()
	{
		if ( _initType == EInitializeType.START )
		{
			Initialize();
		}
	}
	
	// ** PUBLIC
	//
	
	public bool OfflineCreateInstances()
	{
		if ( Application.isPlaying )
		{
			Logger.LogError( _pLogTag + "Offline create not allowed when playing", gameObject );
			return false;
		}
		
		if ( !_isCreateOffline )
		{
			Logger.LogError( _pLogTag + "Offline create not allowed by facade", gameObject );
			return false;
		}
		
		CreateInstances();
		
		return true;
	}
	
	public void Initialize()
	{
		if ( _isInitialized ) return;
		
		if ( !_isCreateOffline )
		{
			CreateInstances();
		}
		
		PoolInstances();
		
		_isInitialized = true;
	}
	
	public GameObject Instantiate()
	{
		GameObject	newInstance = AllocateInstance();
		
		if ( newInstance == null )
		{
			switch ( _onInstantiateFailed )
			{
			case EInstantiateFailedPolicy.USE_OLDEST:
				newInstance = ReallocateOldestInstance();
				break;
			}
		}
		
		return newInstance;
	}
	
	public GameObject Instantiate( Vector3 position, Quaternion rotation )
	{
		GameObject	newInstance = AllocateInstance();
		
		if ( newInstance == null )
		{
			switch ( _onInstantiateFailed )
			{
			case EInstantiateFailedPolicy.USE_OLDEST:
				newInstance = ReallocateOldestInstance();
				break;
			}
		}
		
		if ( newInstance != null )
		{
			newInstance.transform.position = position;
			newInstance.transform.rotation = rotation;
		}
		
		return newInstance;
	}
	
	public void Destroy( GameObject instance )
	{
		FreeInstance( instance );
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	private void CreateInstances()
	{
		List<GameObject>	instances = new List<GameObject>();
		GameObject	newInstance;
		
		for ( int i = 0; i < _prefabs.Length; ++i )
		{
			if ( _prefabs[i] == null ) continue;
			for ( int j = 0; j < _prefabs[i]._count; ++j )
			{
				if ( _prefabs[i]._prefab == null ) continue;
				newInstance = (GameObject)Object.Instantiate( _prefabs[i]._prefab );
				if ( newInstance == null )
				{
					Logger.LogWarning( _pLogTag + "Failed to instantiate instance " + i.ToString() + " from prefab '" + _prefabs[i]._prefab.name + "'", gameObject );
					continue;
				}
				newInstance.transform.parent = this.gameObject.transform;
				if ( _instanceName != null && _instanceName.Length > 0 )
				{
					newInstance.name = _instanceName;
				}
				newInstance.SetActive( false );
				instances.Add( newInstance );
			}
		}
		
		if ( _isShuffleInstances )
		{
			instances.Shuffle();
		}
		
		_instances = instances.ToArray();
	}
	
	private void PoolInstances()
	{
		if ( _instances == null ) return;
		
		int	nextInstance = 0;
		_instancePool = new ObjectPool<GameObject>( _instances.Length, () => ( _instances[ nextInstance++ ] ), null );
	}
	
	private GameObject AllocateInstance()
	{
		if ( !_isInitialized )
		{
			Logger.LogError( _pLogTag + "Not initialized", gameObject );
			return null;
		}
		
		if ( _instancePool == null ) return null;
		
		GameObject	instance = _instancePool.Allocate();
		
		if ( instance == null ) return null;
		
		instance.SetActive( true );
		
		return instance;
	}
	
	private void FreeInstance( GameObject instance )
	{
		instance.SetActive( false );
		
		if ( _instancePool == null ) return;
		
		_instancePool.Free( instance );
	}
	
	private GameObject ReallocateOldestInstance()
	{
		if ( _instancePool == null ) return null;
		
		_instancePool.FreeOldest();
		
		GameObject	instance = _instancePool.Allocate();
		
		if ( instance == null ) return null;
		
		instance.SetActive( true );
		
		return instance;
	}
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
	
	Object IObjectInstantiator.CreateInstance()
	{
		return Instantiate();
	}
	
	void IObjectInstantiator.DestroyInstance( Object inst )
	{
		Destroy( (GameObject)inst );
	}
}

