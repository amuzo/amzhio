﻿////////////////////////////////////////////
// 
// WebGLBootParams.cs
//
// Created: dd/mm/yyyy ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public class WebGLBootParams : InitialisationObject
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[WebGLBootParams] ";

	private const string	DEFAULT_JAVASCRIPT_GET_BOOT_PARAMS = "GetBootParams";

	private const string	DEFAULT_EDITOR_BOOT_PARAMS = "lang=EN";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
#if UNITY_WEBGL
	private static string	_bootParams = null;
#endif

#pragma warning disable 0414
	[SerializeField]
	private string	_editorBootParams = DEFAULT_EDITOR_BOOT_PARAMS;
#pragma warning restore 0414

	//
	// PROPERTIES
	//

#if UNITY_WEBGL
	public static bool _pIsBootParamsReady
	{
		get
		{
			return _bootParams != null;
		}
	}

	public static string _pBootParams
	{
		get
		{
			if ( _bootParams == null )
			{
				LoggerAPI.LogError( "Boot params have not yet been received" );
				return null;
			}

			return _bootParams;
		}
	}

	public static event System.Action	_pOnBootParamsReady;
#endif

	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//
	protected override void Awake()
	{
		base.Awake();
		DontDestroyOnLoad( this.gameObject );

	}

	// ** PUBLIC
	//

	public override void startInitialising()
	{
#if UNITY_WEBGL
		_currentState = InitialisationState.INITIALISING;
		Initialize();
#else
		_currentState = InitialisationState.FINISHED;
#endif
	}

#if UNITY_WEBGL
	public override InitialisationState updateInitialisation() 
	{ 
		if ( _bootParams != null )
		{
			_currentState = InitialisationState.FINISHED;
		}

		return _currentState; 
	}
#endif

	// ** PROTECTED
	//

	// ** PRIVATE
	//

#if UNITY_WEBGL
	private void Initialize()
	{
#if UNITY_EDITOR
		SetBootParams( _editorBootParams );
#else
		RequestBootParams( this.name );
#endif
	}

	private static void SetBootParams( string bootParams )
	{
		_bootParams = bootParams;
		if ( _pOnBootParamsReady != null )
		{
			_pOnBootParamsReady();
		}
	}
	
	private static void RequestBootParams( string instName )
	{
		LoggerAPI.Log( LOG_TAG + "Requesting boot params" );
		Application.ExternalCall( DEFAULT_JAVASCRIPT_GET_BOOT_PARAMS, instName, "OnBootParamsReceived" );
	}

	private void OnBootParamsReceived( string bootParams )
	{
		LoggerAPI.Log( LOG_TAG + "Boot params received: " + bootParams );
		SetBootParams( bootParams ?? string.Empty );
	}
#endif

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

