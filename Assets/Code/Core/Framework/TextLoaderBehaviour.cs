using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class TextLoaderBehaviour : MonoBehaviour
{
	bool _loading = false;
	string _location;
	OnTextLoadComplete _callback;
	object[] _args;

	public void LoadText( string location, OnTextLoadComplete callback, params object[] args )
	{
		if( location != null && location != "" && callback != null )
		{
			if( _loading )
			{
				Debug.LogError( "Cannot load '" + location + "' as still loading '" + _location + "'" );
				return;
			}

			// ASYNCHRONOUS
			if( Application.isPlaying )
			{
				_location = location;
				_callback = callback;
				_args = args;
				_loading = true;

				StartCoroutine( LoadTextCoroutine() );
			}
			// SYNCHRONOUS
			else
			{
				WWW request = new WWW( location );
				System.DateTime start = System.DateTime.Now;
				while( !request.isDone )
				{
					System.TimeSpan lapsed = System.DateTime.Now - start;
					if( lapsed.TotalSeconds > 45 )
					{
						Debug.LogError( "Synchronous Text Load Timeout! -> " + location );
						return;
					}
				}
				callback( request.text, args );
			}
		}
	}

	IEnumerator LoadTextCoroutine()
	{
		WWW request = new WWW( _location );

		yield return request;

		_loading = false;

		_callback( request.text, _args );
	}
}

public delegate void OnTextLoadComplete( string text, params object[] args );