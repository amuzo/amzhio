using UnityEngine;
using AmuzoEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class JsonLoader : InitialisationObject
{
	public TextAsset[] json;
	
	private const float	MAX_ASYNC_ADD_JSON_ACTION_TIME = 0.01f;
	
	private enum EStage
	{
		NULL = 0,
		LOAD,
		ASYNC_ADD,
		READY
	}
	
	private EStage	_stage;
	
	int _loadIndex = 0;
	
#if UNITY_EDITOR
	GameObject _tempEditorLoaderObject;
#endif
	
	public bool	_isAsyncAddJson;
	
	private ActionQueue	_asyncAddJsonActions;
	
	public bool Ready { get { return _stage == EStage.READY; } }
	
	protected override void Awake()
	{
		base.Awake();
		_asyncAddJsonActions = new ActionQueue( gameObject.name + " - AddJson actions" );
		_stage = EStage.NULL;
		//CC20170927 - InitialisationFacade should always exist by this point as base.Awake should have created it
		//if( InitialisationFacade.Instance == null )
		//{
		//	Load();
		//}
	}

	public override void startInitialising ()
	{
		#if DEBUG_MESSAGES
		Debug.Log("JSON Loader: Starting initialization.");
		#endif
		_stage = EStage.LOAD;
		_currentState = InitialisationState.INITIALISING;
		Load();
	}
	
	public override InitialisationState updateInitialisation()
	{
		if ( _stage == EStage.ASYNC_ADD )
		{
			if ( _asyncAddJsonActions == null )
			{
				EndInitialization();
				return _currentState;
			}
			
			_asyncAddJsonActions.DoActions( MAX_ASYNC_ADD_JSON_ACTION_TIME, () => Time.realtimeSinceStartup );
			
			if ( _asyncAddJsonActions._pIsEmpty )
			{
				EndInitialization();
			}
		}
		
		return _currentState;
	}
	
	private void EndInitialization()
	{
		#if DEBUG_MESSAGES
			Debug.Log("JSON Loader: Initialisation Finished.");
		#endif

		_stage = EStage.READY;
		_currentState = InitialisationState.FINISHED;
	}
	
	void Load()
	{
		if(json == null || json.Length == 0)
		{
			return;
		}
		
		if(_loadIndex == json.Length)
		{
			LoadComplete();
			return;
		}
		
		if(_d == null)
		{
			_d = new Dictionary<string, JsonData>();
		}
		
		for(int i = _loadIndex; i<json.Length; ++i)
		{
			TextAsset jsonFile = json[i];
			
			if(jsonFile == null) continue;
			
			string name = jsonFile.name;

#if UNITY_STANDALONE
			Debug.Log("Loading JSON: " + name);
			AddJson(jsonFile.name, jsonFile.text);
			++_loadIndex;
#elif UNITY_WEBPLAYER || UNITY_WEBGL
			// if there is a boot parameter with the same name as this JSON file, load it from there instead
			string url = BootParams.GetBootParam(name); // this could also use the DLCFacade
			if(url != null)
			{
				#if DEBUG_MESSAGES
				Debug.Log("Loading JSON: " + name + " from URL: " + url);
				#endif

				LoadJSON(name, url);
				return;
			}
			else
			{
				#if DEBUG_MESSAGES
				Debug.Log("Loading JSON: " + name);
				#endif
				AddJson(jsonFile.name, jsonFile.text);
				++_loadIndex;
			}
#elif UNITY_IPHONE
			// if there is a file on the hard drive with the same name as this JSON file, load it from there instead
			string streamingFilePath = Application.streamingAssetsPath + "/" + name + ".txt";
			string persistantFilePath = Application.persistentDataPath + "/" + name + ".txt";
			
			if( System.IO.File.Exists( streamingFilePath ) )
			{
				Debug.Log( "Loading JSON [" + name + "] from File [" + streamingFilePath + "]");
				LoadJSON( name, "file://" + streamingFilePath );
				return;
			} 
			else if ( System.IO.File.Exists( persistantFilePath ) )
			{
				Debug.Log( "Loading JSON [" + name + "] from File [" + persistantFilePath + "]");
				LoadJSON( name, "file://" + persistantFilePath );
				return;
			}
			else
			{
				Debug.Log("Loading JSON: " + name);
				AddJson(jsonFile.name, jsonFile.text);
				++_loadIndex;
			}
#elif UNITY_ANDROID
			// if there is a file on the hard drive with the same name as this JSON file, load it from there instead
			string streamingFilePath = Application.streamingAssetsPath + "/" + name + ".txt";
			string persistantFilePath = Application.persistentDataPath + "/" + name + ".txt";
			
			if ( System.IO.File.Exists( persistantFilePath ) )
			{
				Debug.Log( "Loading JSON [" + name + "] from File [" + persistantFilePath + "]");
				LoadJSON( name, persistantFilePath );
				return;
			}
			else 
			{
				Debug.Log( "Loading JSON [" + name + "] from File [" + streamingFilePath + "]");
				LoadJSON( name, streamingFilePath );
				return;
			} 
			// Reading JSON source data from Unity TextAsset is fallback behaviour implemented in OnLoadComplete callback
#else
			Debug.Log("Loading JSON: " + name);
			AddJson(jsonFile.name, jsonFile.text);
			++_loadIndex;
#endif
		}
		
		LoadComplete();
	}
	
	void LoadComplete()
	{
		if ( _isAsyncAddJson )
		{
			_stage = EStage.ASYNC_ADD;
		}
		else
		{
			EndInitialization();
		}
	}
	
	void AddJson(string name, string json)
	{
		System.Action	addAction = () => {
			#if DEBUG_MESSAGES
			Debug.Log("Got JSON: " + name);
			#endif
			_d.Add(name, Extensions.LoadJson(json));
		};
		
		if ( _isAsyncAddJson && _asyncAddJsonActions != null )
		{
			_asyncAddJsonActions.AddAction( addAction );
		}
		else
		{
			addAction();
		}
	}
	
	void LoadJSON(string name, string url)
	{
		++_loadIndex;
		
		if(Application.isPlaying)
		{
			GetComponent<TextLoaderBehaviour>().LoadText(url, OnLoadComplete, name);
		}
#if UNITY_EDITOR
		else
		{
			_tempEditorLoaderObject = new GameObject("_tempEditorLoaderObject");
			_tempEditorLoaderObject.hideFlags = HideFlags.HideInHierarchy;
			TextLoaderBehaviour loader = _tempEditorLoaderObject.AddComponent<TextLoaderBehaviour>();
			loader.LoadText(url, OnLoadComplete, name);
		}
#endif
	}
	
	void OnLoadComplete( string newjson, params object[] args)
	{
		string downloadName = (string)args[0];

		if( string.IsNullOrEmpty( newjson ) )
		{
			Debug.Log("Failed to find local copy of Json [" + downloadName +"]");

			for( int i = 0; i < json.Length; i++ )
			{
				if( json[i] == null || json[i].name != downloadName ) 
				{
					continue;
				}

				AddJson( downloadName, json[i].ToString() );
			}
		}
		else
		{
			AddJson( downloadName, newjson );
		}

		Load();
	}
	
	// static interface
	
	private static Dictionary<string, JsonData> _d = null;
	
	public static JsonData data(string name)
	{
		if(_d == null) return null;
		if (!_d.ContainsKey(name))
		{
			Debug.LogError( "[JsonLoader] Data '" + name + "' not found" );
			return null;
		}
		return _d[name];
	}
	
	public static void Dispose(string name)
	{
		if(_d == null) return;
		_d.Remove(name);
	}
	
	public static void DisposeAll()
	{
		if(_d != null)
		{
			_d.Clear();
			_d = null;
		}
	}
}
