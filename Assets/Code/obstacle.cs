﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstacle : MonoBehaviour {
    private Rigidbody2D rb2d;
    public GameObject Parent;
	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        //float transcale = Random.Range(transform.localScale.x / 2, transform.localScale.x + transform.localScale.x / 2);
        //transform.localScale = new Vector2(transcale, transcale) ;
        Destroy(Parent, 30f);
	}
	
	// Update is called once per frame
	void Update () {
        rb2d.velocity = new Vector2(0, Spawner.instance.PieceSpeed);

    }
}
