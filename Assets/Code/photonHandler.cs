﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class photonHandler : MonoBehaviour {


    private GameObject myPlayer;
    public photonButtons photonB;
    public GameObject Parent;
    public bool PlayerSpawned;

    private void Awake()
    {
       //DontDestroyOnLoad (this.transform);

        SceneManager.sceneLoaded += OnSceneFinishedLoading;
    }
    private void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("scene changed");
        if (scene.name == "MainGame")
        {

            Debug.Log("Loaded the main game scene, time to spawn player");
            if(PlayerSpawned == false)
            {
                spawnPlayer();
            
            }
        }
    

    }


   
    public void createNewRoom()
    {

        GameController.instance.TempName = UISystem.instance.Playername.text;
        PhotonNetwork.JoinRandomRoom();

    }
    public void joinOrCreateRoom()
    {
       // RoomOptions roomOptions = new RoomOptions();
       // roomOptions.MaxPlayers = 4;
        //PhotonNetwork.JoinOrCreateRoom(photonB.joinRoomInput.text, roomOptions, TypedLobby.Default);
    }

    private void OnJoinedRoom()
    {
        Debug.Log("we are connected to a real room, awesome!");
        moveScene();
    }

    public void moveScene()
    {
        Debug.Log("try load main game");
        PhotonNetwork.LoadLevel("MainGame");
    }
   
  

    private void spawnPlayer()
    {
        
            if (PlayerSpawned == true)
             {
            Destroy(myPlayer);
            // var temp = GameObject.FindGameObjectWithTag("mainPlayer");
            //Destroy(temp);
            GameObject myObject = PhotonNetwork.Instantiate("mainPlayer", new Vector3(0, 0, 0), Quaternion.identity, 0);
            Debug.Log("######################### DESTROY PLAYER ######################");
            myPlayer = myObject;



        }
        
        if (PlayerSpawned == false)
        {
            GameObject myObject = PhotonNetwork.Instantiate("mainPlayer", new Vector3(0, 0, 0), Quaternion.identity, 0);
            myPlayer = myObject;
            PlayerSpawned = true;
        }
        Debug.Log("Spawn the player");
      
        //  Destroy(this.gameObject);
        // PhotonNetwork.Instantiate(mainPlayer.name, mainPlayer.transform.position, mainPlayer.rotation, 0);
    }


}
