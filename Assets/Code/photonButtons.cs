﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class photonButtons : MonoBehaviour {
    public InputField createRoomInput, joinRoomInput;

    public photonHandler pHandler;
    // Use this for initialization

 
    public void onClickCreateRoom()
    {
        pHandler.createNewRoom();
        AudioManager.instance.ButtonClickSound();
        AudioManager.instance.DampenMusic = false;

    }

    public void onClickJoinRoom()
    {
        pHandler.joinOrCreateRoom();
    }

   

}
