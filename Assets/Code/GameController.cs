﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
    public static GameController instance;

    public int PlayersBeforeGo;
    public int PreviousScore;
    public int TopScore;

    public GameObject Player1;
    public GameObject Player2;
    public GameObject Player3;
    public GameObject Player4;

    public string Player1Name;
    public string Player2Name;
    public string Player3Name;
    public string Player4Name;



    public string TempName;

    public int Player1_Score;
    public int Player2_Score;
    public int Player3_Score;
    public int Player4_Score;

    public bool TimeToEnd;



    // Use this for initialization

    private void Awake()
    {

        DontDestroyOnLoad(this.transform);
    }
    void Start () {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
    }
}
