﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PhotonConnect : MonoBehaviour {
    public string versionName = "0.3";

    public GameObject sectionview1, sectionview2, sectionview3, PlayButton, InfoText;

    

    private void Start()
    {


        PhotonNetwork.LeaveLobby();
        InfoText.SetActive(false);
        PlayButton.SetActive(true);
        Debug.Log("Clearing Connection");
           // PhotonNetwork.Disconnect();


        Debug.Log("Connecting to photon...");
        PhotonNetwork.ConnectUsingSettings(versionName);


    }

    private void Update()
    {
       if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {

            Debug.Log("Connecting to photon...");
            PhotonNetwork.ConnectUsingSettings(versionName);
        }
    }
    public void connectToPhoton()
    {

        PhotonNetwork.JoinRandomRoom();
    }

    private void OnConnectedToMaster()
    {
  
        // old stuff
        Debug.Log("Connected to master");
        //PhotonNetwork.JoinLobby(TypedLobby.Default);

    }
    private void OnJoinedLobby()
    {

        //oldstuff

       // sectionview1.SetActive(false);
        //sectionview2.SetActive(true);
        Debug.Log("On Join Lobby has worked");
        PhotonNetwork.JoinRandomRoom();
    }

    private void OnPhotonRandomJoinFailed()
    {
        PlayButton.SetActive(false);
        InfoText.SetActive(true);
        UISystem.instance.ScoreTextObj.SetActive(false);
        UISystem.instance.TopScoreTextObj.SetActive(false);

        Debug.Log("We failed to randomly join, trying again in 2 seconds");
        Invoke("TryAgain", 1f * Time.deltaTime);

        //  sectionview3.SetActive(true);

    }



        private void TryAgain()
    {
        RoomOptions roomOptions = new RoomOptions()
        {
            IsVisible = true,
            IsOpen = true,
            MaxPlayers = 4
        };
        PhotonNetwork.CreateRoom(null, roomOptions, TypedLobby.Default);

    }

    private void OnDisconnectedFromPhoton()
    {
        if (sectionview1.activeSelf)
        {
            sectionview1.SetActive(false);
            
        }
        if (sectionview2.activeSelf)
        {
            sectionview2.SetActive(false);

        }
        sectionview3.SetActive(true);
        Debug.Log("Disconnected from PHoton");
    }

    private void OnFailedToConnectToPhoton()
    {
        Debug.Log("Failed to connect, check internet!");
    }



	
	// Update is called once per frame

}
