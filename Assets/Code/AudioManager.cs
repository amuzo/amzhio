﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager instance;
    private static bool created = false;
    public AudioClip bgMusic;

    public float maxVolume;
    public bool DampenMusic;
    //public static AudioClip CollectSound, CollectSound2;


    //public AudioSource Collect;
   public AudioSource Boost;
    public AudioSource PlayButton;
    public AudioSource SmallExplosion;


    private void Awake()


    {
        if (created)
        {
            Destroy(this.gameObject);

        }

        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;

        }



        if (instance == null)
        {
            instance = this;

        }




    }
    // Use this for initialization
    void Start()
    {
        maxVolume = 0;
        DampenMusic = true;
      //  PlayMusic();
    }
    public void ButtonClickSound()
    {
        PlayButton.Play();
    }
    public void PlayBoost()
    {
        Boost.Play();
    }
    /*
        public void PlayStartCollected()
        {
            StarCollected.Play();
        }
        public void PlayCollect()
        {
            Collect.Play();

        }


        public void PlayDoomed()
        {
            Doomed.Play();

        }

        public void PlayAlienDeath()
        {
            AlienDeath.Play();

        }

        */
    public void PlaySmallExplosion()
    {
        SmallExplosion.Play();

    }
    public void setDampenMusic(bool dampen)
    {
        DampenMusic = dampen;

    }

    public void PlayMusic()
    {
        GetComponent<AudioSource>().volume = maxVolume;

        if (!DampenMusic)
        {
            maxVolume = maxVolume + 0.3f * Time.deltaTime;
            if (maxVolume > 0.2f)
            {
                maxVolume = 0.2f;
            }

        }
        if (DampenMusic)
        {
            maxVolume = 0.05f;
            if (GetComponent<AudioSource>().volume > maxVolume)
            {
                GetComponent<AudioSource>().volume -= 1 * Time.deltaTime;

            }
            if (maxVolume < 0.05f)
            {
                maxVolume = 0.05f;
            }

        }

    }

    // Update is called once per frame
    void Update()
    {
        PlayMusic();

    }
}
