﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusController : MonoBehaviour {
    public GameObject Parent;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void TurnOffPickup()
    {
        Destroy(this.gameObject);
    }

    void TurnOffBaddie()
    {
        Destroy(this.gameObject);
    }
}
