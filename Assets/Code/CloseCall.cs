﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseCall : MonoBehaviour {
    public bool triggered;
    public Animator animator;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        animator.SetBool("Triggered", triggered);	
	}

    void CalmDown()
    {
        triggered = false;
    }

  
}
