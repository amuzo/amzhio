﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultsController : MonoBehaviour {

    public Text Player1Score;
    public Text Player2Score;
    public Text Player3Score;
    public Text Player4Score;

	// Use this for initialization
	void Start () {
        Player1Score.text = (GameController.instance.Player1_Score.ToString());
        Player2Score.text = (GameController.instance.Player2_Score.ToString());
        Player3Score.text = (GameController.instance.Player3_Score.ToString());
        Player4Score.text = (GameController.instance.Player4_Score.ToString());


    }

    // Update is called once per frame
    void Update () {
		
	}

   public void PlayAgain()
    {
        SceneManager.LoadScene(0);
    }
}
