﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cleanup : MonoBehaviour {
    public float DestroyDelay;
	// Use this for initialization
	void Start () {
        Destroy(this.gameObject, DestroyDelay);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
