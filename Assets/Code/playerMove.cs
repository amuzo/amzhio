﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class playerMove : Photon.MonoBehaviour {

    public bool devTesting = false;

    public PhotonView photonView;
    public bool spawned = false;



    public Animator animator;
    public bool TriggerAPickup;
    public GameObject BonusSpawn;
    public GameObject BonusObj;
    public GameObject BaddieObj;

    public float TargetYDestination;
    public bool Finished;
    public float NewTargetYPos;
    public string myName;
    public int BoostValue;

    public float moveSpeed = 100f;
    public float jumpForce = 800f;
    public GameObject Hat;
    public Text myScoreText;


    public Vector2 curpos;
    public Vector2 targetVelocity;
    public Rigidbody2D rigidbody;
    public float speed;
    public Vector2 Direction;
    public Vector2 NewDir;
    public float lerptime;
    public float maxVelocity;


    public int myScore;
    public float maxSpeed;
    public Vector2 targetpos;
    private bool Moving;

    private Vector3 selfPos;
    public GameObject screenCam;
    public GameObject plCam;
    public SpriteRenderer sprite;
    public Text plNameText;
    private bool PlayersReady = false;
    private bool assigned = false;
    public SpriteRenderer spriter;
    public ParticleSystem myParticle;
    public ParticleSystem SideParticle1;
    public ParticleSystem SideParticle2;


    public GameObject P_Boost_1;
    public GameObject P_Boost_2;
    public GameObject P_Boost_3;
    public GameObject P_Boost_4;
    public GameObject ParticleParent;
    public GameObject RockBoom;

    public GameObject CircleScale;

    public int CurrentBoostPower;





    private void Awake()
    {
        if (!devTesting && photonView.isMine)
        {
            screenCam = GameObject.Find("Main Camera");
            screenCam.SetActive(false);

            plCam.SetActive(true);
        }
    }

    private void Start()
    {
       
        
        
        targetVelocity = new Vector2(0, 0);
        targetpos = new Vector2(0, 0);
        //  NewTargetYPos = (Screen.height * 0.0005f);
   
        NewTargetYPos = CircleScale.GetComponent<Renderer>().bounds.size.y * 2;
        Debug.Log("Player is " + NewTargetYPos + " tall");

        if (photonView.isMine)
        {


            // Hat.SetActive(true);
        }



        if (Spawner.instance.Player1Joined == false && assigned == false)
        {
            Spawner.instance.Player1Joined = true;



            spriter.color = new Color32(65, 229, 244, 255);

            foreach (Transform child in P_Boost_1.transform)
            {
                var main = P_Boost_1.GetComponentInChildren<ParticleSystem>().main;
                main.startColor = spriter.color;
            }
            foreach (Transform itesm in P_Boost_2.transform)
            {
                var main2 = itesm.GetComponentInChildren<ParticleSystem>().main;
                main2.startColor = spriter.color;
            }

            foreach (Transform itesm in P_Boost_3.transform)
            {
                var main3 = itesm.GetComponentInChildren<ParticleSystem>().main;
                main3.startColor = spriter.color;
            }

            foreach (Transform itesm in P_Boost_4.transform)
            {
                var main4 = itesm.GetComponentInChildren<ParticleSystem>().main;
                main4.startColor = spriter.color;
            }




            Spawner.instance.Players[0] = this.gameObject;
            assigned = true;
        }

        if (Spawner.instance.Player1Joined == true && Spawner.instance.Player2Joined == false && assigned == false)
        {
            Spawner.instance.Player2Joined = true;
            Spawner.instance.Players[1] = this.gameObject;
            assigned = true;



            spriter.color = new Color32(255, 129, 129, 255);

            foreach (Transform child in P_Boost_1.transform)
            {
                var main = P_Boost_1.GetComponentInChildren<ParticleSystem>().main;
                main.startColor = spriter.color;
            }
            foreach (Transform itesm in P_Boost_2.transform)
            {
                var main2 = itesm.GetComponentInChildren<ParticleSystem>().main;
                main2.startColor = spriter.color;
            }

            foreach (Transform itesm in P_Boost_3.transform)
            {
                var main3 = itesm.GetComponentInChildren<ParticleSystem>().main;
                main3.startColor = spriter.color;
            }

            foreach (Transform itesm in P_Boost_4.transform)
            {
                var main4 = itesm.GetComponentInChildren<ParticleSystem>().main;
                main4.startColor = spriter.color;
            }



        }

        if (Spawner.instance.Player2Joined == true && Spawner.instance.Player3Joined == false && assigned == false)
        {
            Spawner.instance.Player3Joined = true;
            Spawner.instance.Players[2] = this.gameObject;
            assigned = true;

            foreach (Transform child in P_Boost_1.transform)
            {
                var main = P_Boost_1.GetComponentInChildren<ParticleSystem>().main;
                main.startColor = spriter.color;
            }
            foreach (Transform itesm in P_Boost_2.transform)
            {
                var main2 = itesm.GetComponentInChildren<ParticleSystem>().main;
                main2.startColor = spriter.color;
            }

            foreach (Transform itesm in P_Boost_3.transform)
            {
                var main3 = itesm.GetComponentInChildren<ParticleSystem>().main;
                main3.startColor = spriter.color;
            }

            foreach (Transform itesm in P_Boost_4.transform)
            {
                var main4 = itesm.GetComponentInChildren<ParticleSystem>().main;
                main4.startColor = spriter.color;
            }

        }

        if (Spawner.instance.Player3Joined == true && Spawner.instance.Player4Joined == false && assigned == false)
        {
            Spawner.instance.Player4Joined = true;
            Spawner.instance.Players[3] = this.gameObject;
            assigned = true;

            spriter.color = Color.green;

            foreach (Transform child in P_Boost_1.transform)
            {
                var main = P_Boost_1.GetComponentInChildren<ParticleSystem>().main;
                main.startColor = spriter.color;
            }
            foreach (Transform itesm in P_Boost_2.transform)
            {
                var main2 = itesm.GetComponentInChildren<ParticleSystem>().main;
                main2.startColor = spriter.color;
            }

            foreach (Transform itesm in P_Boost_3.transform)
            {
                var main3 = itesm.GetComponentInChildren<ParticleSystem>().main;
                main3.startColor = spriter.color;
            }

            foreach (Transform itesm in P_Boost_4.transform)
            {
                var main4 = itesm.GetComponentInChildren<ParticleSystem>().main;
                main4.startColor = spriter.color;
            }


        }
    }


    public void ShiftDown()
    {

        Debug.Log("Move - " + NewTargetYPos);
        // this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y - NewTargetYPos);
        this.TargetYDestination = this.transform.position.y - NewTargetYPos / 2;
        this.BoostValue++;
        this.CurrentBoostPower = this.CurrentBoostPower + 1;

        AudioManager.instance.PlayBoost();



    }

    public void ShiftUp()
    {


        Debug.Log("Move + " + NewTargetYPos);
        if (this.CurrentBoostPower > 0)
        {
            this.CurrentBoostPower = this.CurrentBoostPower - 1;
            this.TargetYDestination = this.transform.position.y + NewTargetYPos * 3;
            this.BoostValue--;

        }
        // this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + NewTargetYPos);
        AudioManager.instance.PlaySmallExplosion();


    }

    private void Update()
    {
        if (plNameText.text.Length < 1)
        {
            myName = GameController.instance.TempName;


        }

      
        //   this.myName = GameController.instance.TempName;
        if (photonView.isMine == false)
            sharename();
        if (CurrentBoostPower <= 1)
        {
            this.CurrentBoostPower = 1;
            this.P_Boost_1.SetActive(true);
            this.P_Boost_2.SetActive(false);
            this.P_Boost_3.SetActive(false);
            this.P_Boost_4.SetActive(false);

        }
        if (CurrentBoostPower == 2)
        {
            this.P_Boost_1.SetActive(false);
            this.P_Boost_2.SetActive(true);
            this.P_Boost_3.SetActive(false);
            this.P_Boost_4.SetActive(false);
        }
        if (CurrentBoostPower == 3)
        {
            this.P_Boost_1.SetActive(false);
            this.P_Boost_2.SetActive(false);
            this.P_Boost_3.SetActive(true);
            this.P_Boost_4.SetActive(false);
        }
        if (CurrentBoostPower >= 4)
        {
            this.CurrentBoostPower = 4;
            this.P_Boost_1.SetActive(false);
            this.P_Boost_2.SetActive(false);
            this.P_Boost_3.SetActive(false);
            this.P_Boost_4.SetActive(true);
        }
        if (GameController.instance.TimeToEnd)
        {
            EndGame();

        }

        if (PlayersReady == false && spawned == true)
        {
            Destroy(this.gameObject);
        }
        if (!PlayersReady)
        {
            if (PhotonNetwork.room.IsOpen == false)
            {
                //  InvokeRepeating("AddScore", 0f, 3f * Time.deltaTime);
                spawned = true;

                PlayersReady = true;
                targetVelocity = new Vector2(0, 0);
                targetpos = new Vector2(Screen.width / 2, 0);

            }



        }

        myScoreText.text = myScore.ToString();
        if (!devTesting)
        {
            if (photonView.isMine) {
                checkInput();
                ManageInput(); }
            else
            {
                smoothNetMovement();
            }
        }
        else
        {
            checkInput();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {


        if (collision.gameObject.CompareTag("CloseCall"))
        {
            myScore = myScore + 1;
            TriggerAPickup = true;
            //  animator.SetBool("GetPoint", TriggerAPickup);
            if (photonView.isMine)
            {

                collision.GetComponent<CloseCall>().triggered = true;

                Instantiate(BonusObj, BonusSpawn.transform.position, Quaternion.identity, BonusSpawn.transform);
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("FinishLine") && this.Finished == false)
        {
            if (GameController.instance.Player1 == null && this.Finished == false)
            {
                GameController.instance.Player1 = this.gameObject;
                GameController.instance.Player1Name = GameController.instance.Player1.GetComponent<playerMove>().myName.ToString();

                this.Finished = true;
            }


            if (GameController.instance.Player2 == null && this.Finished == false)
            {
                GameController.instance.Player2 = this.gameObject;
                GameController.instance.Player2Name = GameController.instance.Player2.GetComponent<playerMove>().myName.ToString();

                this.Finished = true;
            }
            if (GameController.instance.Player3 == null && this.Finished == false)
            {
                GameController.instance.Player3 = this.gameObject;
                GameController.instance.Player3Name = GameController.instance.Player3.GetComponent<playerMove>().myName.ToString();

                this.Finished = true;

            }
            if (GameController.instance.Player4 == null && this.Finished == false)
            {
                GameController.instance.Player4 = this.gameObject;
                GameController.instance.Player4Name = GameController.instance.Player4.GetComponent<playerMove>().myName.ToString();

                this.Finished = true;

            }



            if (GameController.instance.Player1 != null)
            {
            }

            if (GameController.instance.Player2 != null)
            {
            }
            if (GameController.instance.Player3 != null)
            {
            }
            if (GameController.instance.Player4 != null)
            {
            }



            if (photonView.isMine)
                EndGame();
            AudioManager.instance.DampenMusic = true;
        }
    }

    void StopPoint()
    {


    }



    private void FixedUpdate()
    {

        // move up and down depending on boosts / nega-boosts hit 
        if (this.transform.position.y < TargetYDestination)
        {
            this.transform.position = new Vector2(transform.position.x, transform.position.y + 1f * Time.deltaTime);
        }

        if (this.transform.position.y > TargetYDestination)
        {
            this.transform.position = new Vector2(transform.position.x, transform.position.y - 1f * Time.deltaTime);

        }




        //----------------------------------------------------------------//
        //animator.SetBool("GetPoint", TriggerAPickup);
        //  Debug.Log(rb2d.velocity.x);
        //Vector2 newvec = Vector2.Lerp(rb2d.velocity, targetVelocity, Time.deltaTime*lerptime);
        //if (Moving)
        if (PlayersReady)
            rigidbody.AddForce(targetVelocity * speed);

        //Vector2 temp = rb2d.velocity;

        if (rigidbody.velocity.x > maxVelocity)
        {
            rigidbody.velocity = new Vector2(maxVelocity, rigidbody.velocity.y);
        }



        if (rigidbody.velocity.y > maxVelocity)
        {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, maxVelocity);
        }

        if (rigidbody.velocity.x < -maxVelocity)
        {
            rigidbody.velocity = new Vector2(-maxVelocity, rigidbody.velocity.y);
        }



        if (rigidbody.velocity.y < -maxVelocity)
        {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, -maxVelocity);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameController.instance.TimeToEnd = true;
        }



    }
    void ManageInput()
    {
        if (PlayersReady)
        {


            curpos = transform.position;
            if (Input.GetMouseButton(0))
            {
                var mousepos = plCam.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
                Debug.Log("Playerpos is " + transform.position);
                Debug.Log("Touchpos is" + mousepos);
                // targetVelocity = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.transform.position).normalized;
                targetVelocity = (plCam.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition) - this.transform.position).normalized;
                /*
                if (mousepos.x < this.transform.position.x)
                {
              var targetdirection = new Vector3(this.transform.position.x-0.15f, this.transform.position.y,this.transform.position.z);
                    targetVelocity = (targetdirection - this.transform.position).normalized;

                }


                if (mousepos.x > this.transform.position.x)
                {
                    var targetdirection = new Vector3(this.transform.position.x + 0.15f, this.transform.position.y, this.transform.position.z);
                    targetVelocity = (targetdirection - this.transform.position).normalized;

                }
                */


                //targetVelocity = Input.mousePosition - this.transform.position.normalized

                Moving = true;

            }

            if (Input.GetMouseButtonUp(0))
            {
                targetpos = Input.mousePosition;
                targetVelocity = new Vector2(0, 0);
                //targetpos = Input.mousePosition;
                Moving = false;

            }

            if (!Input.GetMouseButton(0))
            {
                Moving = false;
                //targetVelocity = (plCam.GetComponent<Camera>().ScreenToWorldPoint(targetpos) - this.transform.position).normalized;

                //  targetVelocity = (Camera.main.ScreenToWorldPoint(targetpos) - this.transform.position).normalized;
                // targetVelocity = (Input.mousePosition - this.transform.position).normalized;


            }

        }

    }

    private void checkInput()
    {

    }



    public void EndGame()
    {

        //   TellOthersToEnd();
        if (GameController.instance.TopScore < myScore)
        {
            GameController.instance.TopScore = myScore;
        }
        GameController.instance.PreviousScore = myScore;
        DestroyPlayer();
        // Destroy(this.gameObject);
        // PhotonNetwork.DestroyAll();
        // PhotonNetwork.LeaveLobby();
        PhotonNetwork.LeaveRoom();

        PhotonNetwork.LoadLevel(2);
        GameController.instance.TimeToEnd = false;


    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            this.ShiftUp();
            Destroy(collision.gameObject);
            Instantiate(RockBoom, this.transform.position, Quaternion.identity, null);
            collision.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            if (photonView.isMine)
            {


                Instantiate(BaddieObj, BonusSpawn.transform.position, Quaternion.identity, BonusSpawn.transform);
            }


            Debug.Log("Try to destroy Player");
            // EndGame();


            //SceneManager.LoadScene(0);

            // PhotonNetwork.Disconnect();


            // SceneManager.LoadScene(0);
        }

    }




    /// <summary>
    /// nETCODE
    /// </summary>
    /// 


    [PunRPC]
    private void sharename()
    {
       
        //myName = PhotonNetwork..TempName;

    }

    [PunRPC]
        private void TellOthersToEnd()
    {
        GameController.instance.TimeToEnd = true;
    }
    [PunRPC]
    private void DestroyPlayer()
    {
      
            PhotonNetwork.Destroy(this.gameObject);
           // Destroy(this.gameObject);
        


    }
    [PunRPC]
    private void onSpriteFlipTrue()
    {
        sprite.flipX = true;
    }

    [PunRPC]
    private void onSpriteFlipFalse()
    {
        sprite.flipX = false;

    }

    private void smoothNetMovement()
    {
        transform.position = Vector3.Lerp(transform.position, selfPos, Time.deltaTime * 8);
    }

    private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(transform.position);
           // stream.SendNext(myName);
        }
        else
        {
            selfPos = (Vector3)stream.ReceiveNext();

        }
    }
}
