﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : Photon.MonoBehaviour {

    public static ScoreManager instance;
    public GameObject Player1;
    public GameObject Player2;
    public GameObject Player3;
    public GameObject Player4;

    public int Player1_Score;
    public int Player2_Score;
    public int Player3_Score;
    public int Player4_Score;

    public string Player1_Name;
    public string Player2_Name;
    public string Player3_Name;
    public string Player4_Name;

    // Use this for initialization
    void Start () {
		if (instance == null)
        {
            instance = this;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Spawner.instance.startspawning && Player1.gameObject != null)
        {
            Player1_Score = Player1.GetComponent<playerMove>().myScore;
        }

        if (Spawner.instance.startspawning && Player2.gameObject != null)
        {
            Player2_Score = Player2.GetComponent<playerMove>().myScore;
        }

        if (Spawner.instance.startspawning && Player3.gameObject != null)
        {
            Player3_Score = Player3.GetComponent<playerMove>().myScore;
        }
        if (Spawner.instance.startspawning && Player4.gameObject != null)
        {
            Player4_Score = Player4.GetComponent<playerMove>().myScore;
        }
    }
}
