﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostStrips : MonoBehaviour
{
    public bool FastBoost;
    public bool SlowBoost;
    private Rigidbody2D rb2d;
    public bool spent;
    public GameObject boosteffect;
    // Use this for initialization
    void Start()
    {
        rb2d = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rb2d.velocity = new Vector2(0, Spawner.instance.PieceSpeed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (!SlowBoost)
            {
                if (!spent)
                {
                    collision.gameObject.GetComponent<playerMove>().ShiftDown();
                    spent = true;
                    Instantiate(boosteffect, collision.transform.position, Quaternion.identity, null);
                    Destroy(this.gameObject);
                }

            }
            if (SlowBoost)
            {
                if (!spent)
                {
                    collision.gameObject.GetComponent<playerMove>().ShiftUp();
                    spent = true;
                    Destroy(this.gameObject);


                }

            }
        }

       
    }

}

