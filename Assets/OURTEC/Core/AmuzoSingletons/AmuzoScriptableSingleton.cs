﻿////////////////////////////////////////////
// 
// AmuzoScriptableSingleton.cs
//
// Created: 11/12/2014 rbateman
// Contributors: 
// 
// Intention:
// This class is to be the inherited type of all Amuzo Singleton ScriptableObjects.
// This class uses generics to allow singleton access to be inherited removing the 
// need to duplicate the singleton decleration between all singletons.
//
// This class also allows us to have a single point of reference if any additional 
// mangement code needs to be added later down the line.
//
// Note: 
// The expected usage of the class is in the inherited type for future singletons:
// public class AmuzoSaveDataFacade : AmuzoScriptableSingleton<AmuzoSaveDataFacade>
//
// To create the instance of the Scriptable object for serializing in the project, 
// select the new AmuzoScriptableSingleton derived class in the Project view and 
// then select from the Unity Editor "Assets / Create / Instance" and a new 
// serializable object will be created alongside the class.
// 
// To get a reference to this new serialzied class at runtime, 
// please see the AmuzoScriptableObjectReference.cs
//
// Remark: 
// No Remarks.
//
////////////////////////////////////////////

using UnityEngine;

public class AmuzoScriptableSingleton<T> : ScriptableObject where T : ScriptableObject
{
	// CONSTS / DEFINES / ENUMS

	// SINGLETON DECLARATION

	public static bool _pExists
	{
		get
		{
			return _instance != null;
		}
	}

	private static T _instance;
	public static T _pInstance
	{
		get
		{
			if( _instance == null )
			{
				_instance = Resources.Load<T>( typeof(T).Name );

				if( _instance == null )
				{
					_instance = AmuzoScriptableObjectReference.GetScriptableObject<T>();

					if( _instance == null )
					{
						Debug.LogWarning( "AmuzoScriptableSingleton instance of type [" + typeof( T ) + "] couldn't be found. Does it exist? Is it in a Resources folder, or has it been added to the AmuzoScriptableObjectReference?" );
						_instance = CreateInstance<T>();
					}
				}

				if( _instance is AmuzoScriptableSingleton<T> )
				{
					( _instance as AmuzoScriptableSingleton<T> ).Initialise();
				}
			}

			return _instance;
		}
	}

	// NESTED CLASSES / STRUCTS

	// MEMBERS 

	// PROPERTIES

	// ABSTRACTS / VIRTUALS

	protected virtual void Initialise()
	{
	}

	// STATIC FUNCTIONS

	// FUNCTIONS 

	// ** UNITY

	// ** PUBLIC

	// ** PROTECTED

	// ** PRIVATE

	// OPERATOR OVERLOADS
}
