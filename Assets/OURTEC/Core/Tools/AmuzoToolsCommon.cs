﻿////////////////////////////////////////////
// 
// AmuzoToolsCommon.cs
//
// Created: 15/02/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
public static class AmuzoToolsCommon
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[AmuzoToolsCommon] ";

	public const string		TOOLS_MENU_PREFIX = "Amuzo Tools/";

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	//
	// PROPERTIES
	//
	
	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
}

