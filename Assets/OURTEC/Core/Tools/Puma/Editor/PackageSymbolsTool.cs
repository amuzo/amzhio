﻿////////////////////////////////////////////
// 
// PackageSymbolsTool.cs
//
// Created: 03/12/2015 ccuthbert
// Contributors:
// 
// Intention:
// Get define symbols for packages.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PackageSymbolsTool : ScriptableWizard
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[PackageSymbolsTool] ";
	
	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	[MenuItem( ( AmuzoToolsCommon.TOOLS_MENU_PREFIX + "Puma/Package Symbols" ) )]
	static void CreateWizard()
	{
		DisplayWizard<PackageSymbolsTool>( "Package Symbols", "Apply", "Log" );
	}
	
	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	void OnWizardCreate()
	{
		Puma.PackageManager.RefreshDatabase();
		Puma.PackageManager.ApplyPackageSymbols();
	}

	void OnWizardOtherButton()
	{
		Puma.PackageManager.RefreshDatabase();

		string[]	symbols = Puma.PackageManager.GetPackageSymbols();

		LogSymbols( symbols );
	}
	
	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	private void LogSymbols( string[] symbols )
	{
		Debug.Log( "Symbols: " + string.Join( ";", symbols ) );
	}
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

