﻿using UnityEngine;
 using UnityEditor;
 
 static class UsefulShortcuts
 {
	// Aquired from: http://answers.unity3d.com/questions/707636/clear-console-window.html

	[MenuItem ("Amuzo Tools/Shortcuts/Clear Console %#c")] // Win: [CTRL + SHIFT + C] Mac: [CMD + SHIFT + C]
	static void ClearConsole ()
	{
		// This simply does "LogEntries.Clear()" the long way:
		var logEntries = System.Type.GetType( "UnityEditorInternal.LogEntries,UnityEditor.dll" );
		var clearMethod = logEntries.GetMethod( "Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public );
		clearMethod.Invoke( null, null );
     }
 }