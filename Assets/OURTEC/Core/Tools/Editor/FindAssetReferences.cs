﻿////////////////////////////////////////////
// 
// FindAssetReferences.cs
//
// Created: 01/03/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public class FindAssetReferences : EditorWindow
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[FindAssetReferences] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	private UnityEngine.Object	_asset;

	private string	_haystackFilter;

	private List<UnityEngine.Object>	_results;

	private Vector2	_resultsScrollPos;

	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	[MenuItem( AmuzoToolsCommon.TOOLS_MENU_PREFIX + "Find Asset References" )]
	public static void ShowWindow()
	{
		FindAssetReferences window = ( FindAssetReferences )EditorWindow.GetWindow( typeof( FindAssetReferences ) );
		window.titleContent = new GUIContent( "Find Asset References" );
	}

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private void OnGUI()
	{
		EditorGUILayout.BeginVertical();

		_asset = EditorGUILayout.ObjectField( "Asset", _asset, typeof( UnityEngine.Object ), allowSceneObjects:false );

		if ( _asset != null )
		{
			EditorGUILayout.LabelField( "Asset Type", _asset.GetType().ToString() );

			string	assetPath = AssetDatabase.GetAssetPath( _asset );

			EditorGUILayout.LabelField( "Asset Path", assetPath );

			string	assetGuid = AssetDatabase.AssetPathToGUID( assetPath );

			EditorGUILayout.LabelField( "Asset GUID", assetGuid );

			_haystackFilter = EditorGUILayout.TextField( "Filter", _haystackFilter );

			if ( GUILayout.Button( "Find" ) )
			{
				_results = new List<UnityEngine.Object>();

				List<string>	haystackGuids = new List<string>( AssetDatabase.FindAssets( _haystackFilter ) );

				for ( int i = 0; i < haystackGuids.Count; ++i )
				{
					for ( int j = i + 1; j < haystackGuids.Count; ++j )
					{
						if ( haystackGuids[i] == haystackGuids[j] )
						{ 
							haystackGuids.RemoveAt( j-- );
						}
					}
				}

				string		haystackPath;

				foreach ( string haystackGuid in haystackGuids )
				{
					haystackPath = AssetDatabase.GUIDToAssetPath( haystackGuid );

					if ( Directory.Exists( haystackPath ) ) continue;

					if ( DoesFileContainReference( haystackPath, assetGuid ) )
					{
						_results.Add( AssetDatabase.LoadMainAssetAtPath( haystackPath ) );
					}
				}
			}

			if ( _results != null )
			{
				_resultsScrollPos = EditorGUILayout.BeginScrollView( _resultsScrollPos );

				foreach ( UnityEngine.Object result in _results )
				{
					EditorGUILayout.ObjectField( result, typeof( UnityEngine.Object ), allowSceneObjects:false );
				}

				EditorGUILayout.EndScrollView();
			}
		}

		EditorGUILayout.EndVertical();
	}

	private static bool DoesFileContainReference( string filePath, string guid )
	{
		string	line;
		int		lineIdx = 0;
		int		charIdx = -1;

		using ( StreamReader sr = new StreamReader( filePath ) )
		{
			char[]	firstBytes = new char[5];
			if ( sr.ReadBlock( firstBytes, 0, 5 ) < 5 ) return false;
			if ( new string( firstBytes ) != "%YAML" ) return false;

			for ( line = sr.ReadLine(); line != null && ( charIdx = line.IndexOf( guid, System.StringComparison.OrdinalIgnoreCase ) ) < 0; line = sr.ReadLine(), ++lineIdx ) { }
			sr.Close();
		}
		if ( line != null )
		{
			Debug.Log( LOG_TAG + string.Format( "GUID found: {0}({1}, {2}): {3}", filePath, lineIdx, charIdx, line ) );
		}
		return line != null;
	}
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

