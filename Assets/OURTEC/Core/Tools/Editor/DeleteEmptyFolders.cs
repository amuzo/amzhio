﻿////////////////////////////////////////////
// 
// DeleteEmptyFolders.cs
//
// Created: 13/01/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.IO; 
using UnityEditor;
using UnityEngine;
using AmuzoEngine;
using Logger = UnityEngine.Debug;
#if DEBUG_MESSAGES
using Verbose = UnityEngine.Debug;
#else
using Verbose = NoDebugLogger;
#endif

public class DeleteEmptyFolders : ScriptableWizard
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[DeleteEmptyFolders] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	[MenuItem ("Amuzo Tools/Delete Empty Folders")]
	static void CreateWizard()
	{
		ScriptableWizard.DisplayWizard<DeleteEmptyFolders>( "Delete Empty Folders", "Delete", "Dry Run" );
	}
	
	public static void ProcessFolder( string folder )
	{
		ProcessFolder( folder, f => {
			if ( !AssetDatabase.DeleteAsset( f ) )
			{
				Logger.LogWarning( LOG_TAG + "Failed to delete folder: " + f );
			}
		 } );
	}

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	void OnWizardCreate()
	{
		ProcessFolder( "Assets" );
	}

	void OnWizardOtherButton()
	{
		ProcessFolder( "Assets", f => {
			Logger.Log( LOG_TAG + "Delete folder: " + f );
		} );
	}

	// ** PUBLIC
	//

	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	private static void ProcessFolder( string folder, System.Action<string> folderAction )
	{
		if ( folderAction == null ) return;

		string[]	subFolders = Directory.GetDirectories( folder );

		for ( int i = 0; i < subFolders.Length; ++i )
		{
			ProcessFolder( subFolders[i], folderAction );
		}

		string[]	contents = Directory.GetFileSystemEntries( folder );

		if ( contents.Length == 0 )
		{
			folderAction( folder );
		}
	}

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

