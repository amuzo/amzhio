﻿////////////////////////////////////////////
// 
// CameraState.cs
//
// Created: 05/06/2018 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_CameraState
#endif

using UnityEngine;

namespace AmuzoEngine
{
	[System.Serializable]
	public class CameraState
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[CameraState] ";

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//
			
		//
		// MEMBERS 
		//

		[SerializeField]
		private Vector3	_position;

		[SerializeField]
		private Quaternion	_rotation;

		[SerializeField]
		private float	_nearClip;

		[SerializeField]
		private float	_farClip;

		[SerializeField]
		private float	_fieldOfView;
	
		//
		// PROPERTIES
		//

		public Vector3 _pPosition
		{
			get
			{
				return _position;
			}
			set
			{
				_position = value;
			}
		}

		public Quaternion _pRotation
		{
			get
			{
				return _rotation;
			}
			set
			{
				_rotation = value;
			}
		}

		public float _pNearClip
		{
			get
			{
				return _nearClip;
			}
			set
			{
				_nearClip = value;
			}
		}

		public float _pFarClip
		{
			get
			{
				return _farClip;
			}
			set
			{
				_farClip = value;
			}
		}

		public float _pFieldOfView
		{
			get
			{
				return _fieldOfView;
			}
			set
			{
				_fieldOfView = value;
			}
		}
	
		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// FUNCTIONS 
		//

		// ** UNITY
		//

		// ** PUBLIC
		//

		public static implicit operator CameraState( Camera camera )
		{
			return new CameraState( camera );
		}

		public CameraState( CameraState sourceState )
		{
			CopyFrom( sourceState );
		}

		public CameraState( Camera sourceCamera )
		{
			CopyFrom( sourceCamera );
		}

		public void CopyFrom( CameraState sourceState )
		{
			_position = sourceState._position;
			_rotation = sourceState._rotation;
			_nearClip = sourceState._nearClip;
			_farClip = sourceState._farClip;
			_fieldOfView = sourceState._fieldOfView;
		}

		public void CopyFrom( Camera sourceCamera )
		{
			_position = sourceCamera.transform.position;
			_rotation = sourceCamera.transform.rotation;
			_nearClip = sourceCamera.nearClipPlane;
			_farClip = sourceCamera.farClipPlane;
			_fieldOfView = sourceCamera.fieldOfView;
		}

		public void CopyTo( Camera targetCamera )
		{
			targetCamera.transform.position =_position;
			targetCamera.transform.rotation = _rotation;
			targetCamera.nearClipPlane = _nearClip;
			targetCamera.farClipPlane = _farClip;
			targetCamera.fieldOfView = _fieldOfView;
		}

		public void Lerp( CameraState startState, CameraState endState, float t )
		{
			this._position = Vector3.Lerp( startState._position, endState._position, t );
			this._rotation = Quaternion.Slerp( startState._rotation, endState._rotation, t );
			this._nearClip = Mathf.Lerp( startState._nearClip, endState._nearClip, t );
			this._farClip = Mathf.Lerp( startState._farClip, endState._farClip, t );
			this._fieldOfView = Mathf.Lerp( startState._fieldOfView, endState._fieldOfView, t );
		}
	
		// ** INTERNAL
		//
	
		// ** PROTECTED
		//

		// ** PRIVATE
		//
	
		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
