﻿////////////////////////////////////////////
// 
// NullableWeakRef.cs
//
// Created: 30/11/2016 ccuthbert
// Contributors:
// 
// Intention:
// Generic weak ref which allows setting null.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
using System;
using LoggerAPI = UnityEngine.Debug;

public class NullableWeakRef<T> where T : class
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[NullableWeakRef] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	private WeakReference	_ref;
	
	//
	// PROPERTIES
	//

	public T _pTarget
	{
		get
		{
			return _ref != null ? (T)_ref.Target : null;
		}
		set
		{
			if ( _ref == null )
			{
				_ref = new WeakReference( value );
			}
			else
			{
				_ref.Target = value;
			}
		}
	}
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

