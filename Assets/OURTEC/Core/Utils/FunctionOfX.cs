﻿////////////////////////////////////////////
// 
// FunctionOfX.cs
//
// Created: 22/09/2016 rbateman
// Contributors: 
// 
// Intention:
// This class is to describe an equation like "x^3 - 3x^2 + 10" while being inspector serializable
// and human readable ( with the help of the Editor class FunctionOfXDescription )
//
// Note: 
// No Notes.
//
// Remark: 
// No Remarks
//
////////////////////////////////////////////

using UnityEngine;
using System.Collections;

[System.Serializable]
public class FunctionOfX
{
	[System.Serializable]
	public class FunctionSegment
	{
		public float _multiplier;
		public int _powerInt;
		public bool _doUsePowerFloat;
		public float _powerFloat;
	}

	public class FunctionOfXDescriptionAttribute : PropertyAttribute
	{
		public FunctionOfXDescriptionAttribute () {}
	}

	public FunctionSegment[] _segments;
	
	// This is a dummy variable to show our custom inspector drawer.
	[FunctionOfXDescription]
	public string _functionDescriptionString;

	private float returnValue;
	private float tempPow;
	private int x;
	private int y;

	public float Run( float forX )
	{
		returnValue = 0f;

		for( x = 0; x < _segments.Length; x++ )
		{
			if( _segments[x]._doUsePowerFloat )
			{
				if( _segments[x]._powerFloat == 0f )
				{
					tempPow = 1f;
				}
				else if( _segments[x]._powerFloat == 1f )
				{
					tempPow = forX;
				}
				else
				{
					// Mathf.Pow is made for using with decimals. 
					// It's not reccommended to use it under any other circumstances for performance reasons. 
					tempPow = Mathf.Pow( forX, _segments[x]._powerFloat );
				}
			}
			else
			{
				tempPow = 1f;
				for( y = 0; y < _segments[x]._powerInt; y++ )
				{
					tempPow *= forX;
				}
			}
			
			returnValue += _segments[x]._multiplier * tempPow;
		}

		return returnValue;
	}

	public static string MakeFunctionDescriptionString( FunctionSegment[] segments )
	{
		if( segments == null || segments.Length == 0 )
		{
			return "0";
		}

		System.Array.Sort( segments, sortSegmentByPower );

		System.Text.StringBuilder sb = new System.Text.StringBuilder();

		for( int x = 0; x < segments.Length; x++ )
		{
			if( segments[x] == null )
			{
				continue;
			}

			if( segments[x]._multiplier >= 0f && x != 0 )
			{
				sb.Append( "+ " );
			}

			if( segments[x]._multiplier != 1f ||
				( segments[x]._doUsePowerFloat && segments[x]._powerFloat == 0f ) ||
				( !segments[x]._doUsePowerFloat && segments[x]._powerInt == 0 ) ) 
			{
				sb.Append( segments[x]._multiplier.ToString() );
			}

			if( ( segments[x]._doUsePowerFloat && segments[x]._powerFloat != 0f ) ||
				( !segments[x]._doUsePowerFloat && segments[x]._powerInt != 0 ) )
			{
				sb.Append( "x" );

				if( ( segments[x]._doUsePowerFloat && segments[x]._powerFloat != 1f ) ||
				( !segments[x]._doUsePowerFloat && segments[x]._powerInt != 1 ) )
				{
					sb.Append( "^" );

					if( segments[x]._doUsePowerFloat )
					{
						sb.Append( segments[x]._powerFloat );
					}
					else
					{
						sb.Append( segments[x]._powerInt );
					}
				}
			}

			if( x != segments.Length - 1 )
			{
				sb.Append( " " );
			}
		}

		return sb.ToString();
	}

	private static int sortSegmentByPower( FunctionSegment segA, FunctionSegment segB )
	{
		if( segA == null && segB == null )
		{
			return 0;
		}
		else if( segA == null )
		{
			return 1;
		}
		else if ( segB == null )
		{
			return -1;
		}

		float valueA = segA._doUsePowerFloat? segA._powerFloat : segA._powerInt;
		float valueB = segB._doUsePowerFloat? segB._powerFloat : segB._powerInt;

		if( valueA == valueB )
		{
			return 0;
		}

		return valueA < valueB ? 1 : -1;
	}
}
