﻿////////////////////////////////////////////
// 
// Utils.cs
//
// Created: 27/09/2017 ccuthbert
// Contributors:
// 
// Intention:
// Safe utils from the other Utils.cs file, 
// which can be included in CoreUtils because 
// they have no non-CoreUtils dependencies.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public static partial class Utils
{
	public static Texture2D GetSolidTexture( Color colour, int width = 1, int height = 1 )
	{
		Texture2D ret = new Texture2D( width, height );

		for( int x = 0; x < width; ++x )
		{
			for( int y = 0; y < height; ++y )
			{
				ret.SetPixel( x, y, colour );
			}
		}

		ret.Apply();

		return ret;
	}

	public static string FormatLikeInspectorVariable( object str )
	{
		string ret = "";

		bool isFirstLetter = true;
		bool wasPrevNewWordChar = false;

		foreach( char ch in str.ToString() )
		{
			if( isFirstLetter && !char.IsLetter( ch ) && !char.IsNumber( ch ) )
			{
				continue;
			}

			bool isNewWordChar = char.IsUpper( ch ) || char.IsNumber( ch );

			ret +=
				isFirstLetter
					? ( char.ToUpper( ch ).ToString() )
					: ( isNewWordChar && !wasPrevNewWordChar ? " " : "" ) + ch;

			wasPrevNewWordChar = isNewWordChar || ch == '_';
			isFirstLetter = false;
		}

		return ret;
	}

}

