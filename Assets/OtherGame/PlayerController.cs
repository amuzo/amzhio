﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerController : MonoBehaviour {

    public static PlayerController instance;
    public Vector2 curpos;
    public Vector2 targetVelocity;
    private Rigidbody2D rb2d;
    public float speed;
    public Vector2 Direction;
    public Vector2 NewDir;
    public float lerptime;
    public float maxVelocity;
    public float currentPoints;
    // Use this for initialization
    void Start () {
        if (instance == null)
        {
            instance = this;
        }
        rb2d = GetComponent<Rigidbody2D>();
        InvokeRepeating("AddPoints", 0f, 25f*Time.deltaTime);

    }

    void AddPoints()
    {
        currentPoints++;

    }
    // Update is called once per frame
    void Update () {
        curpos = transform.position;
        if (Input.GetMouseButton(0))
        {
            targetVelocity = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.transform.position).normalized;


        }



        if (Input.GetMouseButtonUp(0))
        {

           // targetVelocity = new Vector2(0, 0);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            SceneManager.LoadScene(0);
        }
    }
    private void FixedUpdate()
    {
        //  Debug.Log(rb2d.velocity.x);
        //Vector2 newvec = Vector2.Lerp(rb2d.velocity, targetVelocity, Time.deltaTime*lerptime);
        rb2d.AddForce(targetVelocity * speed);

        //Vector2 temp = rb2d.velocity;

        if (rb2d.velocity.x > maxVelocity)
        {
            rb2d.velocity = new Vector2(maxVelocity, rb2d.velocity.y);
        }



        if (rb2d.velocity.y > maxVelocity)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, maxVelocity);
        }

        if (rb2d.velocity.x < -maxVelocity)
        {
            rb2d.velocity = new Vector2(-maxVelocity, rb2d.velocity.y);
        }



        if (rb2d.velocity.y < -maxVelocity)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, -maxVelocity);
        }




    }
}
