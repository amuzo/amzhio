﻿////////////////////////////////////////////
// 
// UwpUtils.cs
//
// Created: 10/03/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
using System.Runtime.InteropServices;
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public static class UwpUtils
{
#if UNITY_WSA && !UNITY_EDITOR
    [DllImport( EXE_NAME )]
    private static extern bool AmuzoUwpUtils_IsDeviceMobile();

    [DllImport( EXE_NAME )]
    private static extern bool AmuzoUwpUtils_IsDeviceDesktop();
#endif

 	//
	// CONSTS / DEFINES / ENUMS
	//

	private const string	PRODUCT_CODE = "AMZQUZ";

	private const string	EXE_NAME = PRODUCT_CODE + ".exe";
	
	private const string	LOG_TAG = "[UwpUtils] ";

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	//
	// PROPERTIES
	//

	public static bool _pIsDeviceMobile
	{
		get
		{
#if UNITY_WSA
	#if UNITY_EDITOR
			return false;
	#else
			bool	result = false;
			UnityEngine.WSA.Application.InvokeOnUIThread( () => {
				result = AmuzoUwpUtils_IsDeviceMobile();
			}, waitUntilDone:true );
			Debug.Log( "_pIsDeviceMobile: " + result.ToString() );
			return result;
	#endif
#else
			return false;
#endif
		}
	}

	public static bool _pIsDeviceDesktop
	{
		get
		{
#if UNITY_WSA
	#if UNITY_EDITOR
			return true;
	#else
			bool	result = false;
			UnityEngine.WSA.Application.InvokeOnUIThread( () => {
				result = AmuzoUwpUtils_IsDeviceDesktop();
			}, waitUntilDone:true );
			Debug.Log( "_pIsDeviceDesktop: " + result.ToString() );
			return result;
	#endif
#else
			return false;
#endif
		}
	}
	
	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
}

