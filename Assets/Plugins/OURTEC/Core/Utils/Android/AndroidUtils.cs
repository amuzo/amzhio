﻿////////////////////////////////////////////
// 
// AndroidUtils.cs
//
// Created: 04/08/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if UNITY_ANDROID
using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using LoggerAPI = UnityEngine.Debug;

namespace AmuzoEngine
{
	public static class AndroidUtils
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[AndroidUtils] ";

		//
		// NESTED CLASSES / STRUCTS
		//
			
		//
		// MEMBERS 
		//

		//
		// PROPERTIES
		//
	
		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//
	
		public static bool IsPermissionGranted( string perm )
		{
			AndroidJavaClass	unityPlayerClass = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" );
			AndroidJavaObject	activity = unityPlayerClass.GetStatic<AndroidJavaObject>( "currentActivity" );
			AndroidJavaClass	contextCompatClass = new AndroidJavaClass( "android.support.v4.content.ContextCompat" );

			int	permsStatus = contextCompatClass.CallStatic<int>( "checkSelfPermission", activity, perm );

			return permsStatus == 0;
		}

		public static void RequestPermissions( IList<string> perms, int requestId )
		{
			AndroidJavaClass	clsUnity = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" );
			AndroidJavaObject	objActivity = clsUnity.GetStatic<AndroidJavaObject>( "currentActivity" );

			string[]	permsArray = new string[ perms.Count ];

			perms.CopyTo( permsArray, 0 );

			LoggerAPI.Log( LOG_TAG + string.Format( "Calling requestPermissions with {0} perms ({1}, ...), and request ID {2}", permsArray.Length, permsArray[0], requestId ) );
			objActivity.Call( "requestPermissions", permsArray, requestId );
		}

		public static bool AreNotificationsEnabled()
		{
			//CC20171101
			//based on following java code
			//NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(UnityPlayer.currentActivity);
			//return notificationManagerCompat.areNotificationsEnabled();

			AndroidJavaClass	unityPlayerClass = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" );
			AndroidJavaObject	activity = unityPlayerClass.GetStatic<AndroidJavaObject>( "currentActivity" );
			AndroidJavaClass	managerCompatClass = new AndroidJavaClass( "android.support.v4.app.NotificationManagerCompat" );
			AndroidJavaObject	managerCompat = managerCompatClass.CallStatic<AndroidJavaObject>( "from", activity );

			return managerCompat.Call<bool>( "areNotificationsEnabled" );
		}

		//From https://forum.unity3d.com/threads/unique-identifier-details.353256/
		public static string GetDeviceUniqueIdentifier_ReadPhoneState( bool oldBehaviour = true )
		{
			string id = "";
			AndroidJavaClass jc = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" );
			AndroidJavaObject activity = jc.GetStatic<AndroidJavaObject>( "currentActivity" );
			AndroidJavaClass contextClass = new AndroidJavaClass( "android.content.Context" );
			string TELEPHONY_SERVICE = contextClass.GetStatic<string>( "TELEPHONY_SERVICE" );
			AndroidJavaObject telephonyService = activity.Call<AndroidJavaObject>( "getSystemService", TELEPHONY_SERVICE );
			bool noPermission = false;

			try
			{
				id = telephonyService.Call<string>( "getDeviceId" );
				//Debug.Log(LOG_TAG + "Getting Id from DEVICE_ID");
			}
			catch( Exception )
			{
				noPermission = true;
			}

			if( id == null )
				id = "";

			// <= 4.5 : If there was a permission problem, we would not read Android ID
			// >= 4.6 : If we had permission, we would not read Android ID, even if null or "" was returned
			if( ( noPermission && !oldBehaviour ) || ( !noPermission && id == "" && oldBehaviour ) )
			{
				//Debug.Log(LOG_TAG + "Getting Id from ANDROID_ID");
				AndroidJavaClass settingsSecure = new AndroidJavaClass( "android.provider.Settings$Secure" );
				string ANDROID_ID = settingsSecure.GetStatic<string>( "ANDROID_ID" );
				AndroidJavaObject contentResolver = activity.Call<AndroidJavaObject>( "getContentResolver" );
				id = settingsSecure.CallStatic<string>( "getString", contentResolver, ANDROID_ID );
				if( id == null )
					id = "";
			}
			if( id == "" )
			{
				//Debug.Log(LOG_TAG + "Getting Id from MAC_ADDRESS");
				string mac = "00000000000000000000000000000000";
				try
				{
					StreamReader reader = new StreamReader( "/sys/class/net/wlan0/address" );
					mac = reader.ReadLine();
					reader.Close();
				}
				catch( Exception ) { }
				id = mac.Replace( ":", "" );
			}
			return GetMd5Hash( id );
		}

		// ** PROTECTED
		//

		// ** PRIVATE
		//

		// Hash an input string and return the hash as
		// a 32 character hexadecimal string.
		private static string GetMd5Hash( string input )
		{
			if( input == "" )
				return "";

			MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
			byte[] data = md5Hasher.ComputeHash( Encoding.Default.GetBytes( input ) );
			StringBuilder sBuilder = new StringBuilder();
			for( int i = 0; i < data.Length; i++ )
				sBuilder.Append( data[i].ToString( "x2" ) );
			return sBuilder.ToString();
		}
	}
}
#endif
