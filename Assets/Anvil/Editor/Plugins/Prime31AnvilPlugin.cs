﻿////////////////////////////////////////////
// 
// Prime31AnvilPlugin.cs
//
// Created: 18/02/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !UNITY_4_6
#define USE_INITIALIZE_ON_LOAD_METHOD
#endif

using System.Collections.Generic; 
using System.Xml;
using UnityEngine;
using UnityEditor;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

#if !USE_INITIALIZE_ON_LOAD_METHOD
[InitializeOnLoad]
#endif
public class Prime31AnvilPlugin : AnvilBuild.IBuildPlugin
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[Prime31AnvilPlugin] ";

	private const string	PRIME31_ANDROID_MAIN_ACTIVITY_NAME = "com.prime31.UnityPlayerNativeActivity";

	//
	// SINGLETON DECLARATION
	//

	private static Prime31AnvilPlugin	_instance;

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

#if !USE_INITIALIZE_ON_LOAD_METHOD
	static Prime31AnvilPlugin()
	{
		CreateSingleton();
	}
#endif

#if USE_INITIALIZE_ON_LOAD_METHOD
	[InitializeOnLoadMethod]
#endif
	private static void CreateSingleton()
	{
		_instance = new Prime31AnvilPlugin();

		AnvilBuild.RegisterBuildPlugin( _instance );
	}

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	//
	// INTERFACE IMPLEMENTATIONS
	//

	string AnvilBuild.IBuildPlugin._pPluginName
	{
		get
		{
			return "Prime31";
		}
	}

	bool AnvilBuild.IBuildPlugin._pIsRequired
	{
		get
		{
#if PUMA_DEFINE_PRIME31
			return true;
#else
			return false;
#endif
		}
	}

	void AnvilBuild.IBuildPlugin.OnPreBuild()
	{
	}

	bool AnvilBuild.IBuildPlugin.OnSetAndroidMainActivity( ref string mainActivityName )
	{
		mainActivityName = PRIME31_ANDROID_MAIN_ACTIVITY_NAME;
		return true;
	}

    //<activity android:name="com.prime31.UnityPlayerNativeActivity" android:label="@string/app_name" android:configChanges="fontScale|keyboard|keyboardHidden|locale|mnc|mcc|navigation|orientation|screenLayout|screenSize|smallestScreenSize|uiMode|touchscreen">
    //  <meta-data android:name="unityplayer.ForwardNativeEventsToDalvik" android:value="true" />
    //</activity>
	void AnvilBuild.IBuildPlugin.OnSetAndroidManifestEntries( XmlDocument manifest, XmlElement rootElem )
	{
		XmlUtils.SetXmlChild( rootElem, "application", applicationElem => {
			AnvilBuild.SetAndroidManifestElement( manifest, applicationElem, "activity", "name", PRIME31_ANDROID_MAIN_ACTIVITY_NAME, activityElem => {
				activityElem.SetAttribute( "label", AnvilBuild._pAndroidManifestNamespaceURI, "@string/app_name" );
				activityElem.SetAttribute( "configChanges", AnvilBuild._pAndroidManifestNamespaceURI, "fontScale|keyboard|keyboardHidden|locale|mnc|mcc|navigation|orientation|screenLayout|screenSize|smallestScreenSize|uiMode|touchscreen" );
				AnvilBuild.SetAndroidManifestElement( manifest, activityElem, "meta-data", "name", "unityplayer.ForwardNativeEventsToDalvik", dataElem => {
					dataElem.SetAttribute( "value", AnvilBuild._pAndroidManifestNamespaceURI, "true" );
				} );
			} );
		} );
	}

	void AnvilBuild.IBuildPlugin.OnSetIosPlistEntries( XmlDocument plist, XmlElement rootDict )
	{
	}
}

